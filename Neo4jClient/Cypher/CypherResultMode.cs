﻿namespace Neo4jClient.Cypher
{
    public enum CypherResultMode
    {
        Set,
        Projection
    }
}
